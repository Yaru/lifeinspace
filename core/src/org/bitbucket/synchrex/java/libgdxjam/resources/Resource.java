package org.bitbucket.synchrex.java.libgdxjam.resources;

public class Resource {

    private String name;
    private int monetaryValue;
    private int resistance;
    private Boolean canGenerateRandomly;
    private Boolean canBeSold;
    private int quantity = 1;
    private float rarity;

    public String getName() {
        return name;
    }

    public Resource setName(String name) {
        this.name = name;
        return this;
    }

    public int getMonetaryValue() {
        return monetaryValue;
    }

    public Resource setMonetaryValue(int monetaryValue) {
        this.monetaryValue = monetaryValue;
        return this;
    }

    public int getResistance() {
        return resistance;
    }

    public Resource setResistance(int resistance) {
        this.resistance = resistance;
        return this;
    }

    public Boolean getCanGenerateRandomly() {
        return canGenerateRandomly;
    }

    public Resource setCanGenerateRandomly(Boolean canGenerateRandomly) {
        this.canGenerateRandomly = canGenerateRandomly;
        return this;
    }

    public Boolean getCanBeSold() {
        return canBeSold;
    }

    public Resource setCanBeSold(Boolean canBeSold) {
        this.canBeSold = canBeSold;
        return this;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public Resource setQuantity(int q)
    {
        quantity = q;
        return this;
    }

    public float getRarity()
    {
        return rarity;
    }

    /**
     *
     * @param r 0.1 - 0.9
     * @return
     */
    public Resource setRarity(float r)
    {
        rarity = r;
        return this;
    }
}

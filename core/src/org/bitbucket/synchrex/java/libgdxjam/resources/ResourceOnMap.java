package org.bitbucket.synchrex.java.libgdxjam.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;
import org.bitbucket.synchrex.java.libgdxjam.utils.ColorUtil;

import java.awt.*;
import java.util.Random;

public class ResourceOnMap {

    Sprite sprite;
    TextureAtlas atlas;
    float alpha;

    public ResourceOnMap(float X, float Y,AssetManager assetManager) {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("square");
        sprite = new Sprite(region);
        Color color = ColorUtil.getRandomColor();
        sprite.setColor(color.getRed(),color.getGreen(),color.getBlue(),1);
        alpha = 1;
        setPosition(X,Y);
    }

    public void draw(Batch batch) {
        if (alpha > 0.2) {
            alpha-= Gdx.graphics.getDeltaTime();
        }
        else {
            alpha = 1;
        }
        sprite.draw(batch);
        sprite.setAlpha(alpha);

    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void dispose() {
        atlas.dispose();
    }
}

package org.bitbucket.synchrex.java.libgdxjam.resources;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class ResourceList {
    private static Array<Resource> resourceArray;

    public static void init()
    {
        resourceArray = new Array<Resource>();
        resourceArray.add(new Resource().setName("UNOBTANIUM").setMonetaryValue(100).setCanGenerateRandomly(true).setResistance(10).setCanBeSold(false).setRarity(1));
        resourceArray.add(new Resource().setName("STEVENSITE").setMonetaryValue(2000).setCanGenerateRandomly(true).setResistance(10).setCanBeSold(true).setRarity(0.8f));
        resourceArray.add(new Resource().setName("JOHNSMITHIUM").setMonetaryValue(100).setCanGenerateRandomly(true).setResistance(1).setCanBeSold(true).setRarity(0.5f));
        resourceArray.add(new Resource().setName("TARDISITE").setMonetaryValue(3000).setCanGenerateRandomly(true).setResistance(10).setCanBeSold(true).setRarity(0.2f));
        resourceArray.add(new Resource().setName("NOTCHSITE").setMonetaryValue(1000).setCanGenerateRandomly(true).setResistance(20).setCanBeSold(true).setRarity(0.7f));
        resourceArray.add(new Resource().setName("METTATONIUM").setMonetaryValue(5000).setCanGenerateRandomly(true).setResistance(15).setCanBeSold(true).setRarity(0.8f));
        resourceArray.add(new Resource().setName("MCGUFFINSITE").setMonetaryValue(10).setCanGenerateRandomly(true).setResistance(1).setCanBeSold(false).setRarity(0.2f));
        resourceArray.add(new Resource().setName("CIPHERITIUM").setMonetaryValue(10000).setCanGenerateRandomly(true).setResistance(30).setCanBeSold(true).setRarity(0.1f));
    }

    public static Resource getRandomResource()
    {
        Resource r = resourceArray.get(MathUtils.random(resourceArray.size - 1));

        return new Resource().setName(r.getName()).setMonetaryValue(r.getMonetaryValue()).setCanGenerateRandomly(r.getCanGenerateRandomly()).setCanBeSold(r.getCanBeSold()).setRarity(r.getRarity()).setResistance(r.getResistance());
    }

    public static Array<Resource> getAllResources()
    {
        return resourceArray;
    }
}


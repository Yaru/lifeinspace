package org.bitbucket.synchrex.java.libgdxjam;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import org.bitbucket.synchrex.java.libgdxjam.screens.LoadingScreen;
import org.bitbucket.synchrex.java.libgdxjam.screens.MainScreen;

public class LibGDXJam extends Game {
	SpriteBatch batch;
    BitmapFont font;
    AssetManager assetManager;

	@Override
	public void create () {
		batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        assetManager = new AssetManager();
        setScreen(new LoadingScreen(this));
	}

    public SpriteBatch getBatch() {
        return batch;
    }

    public BitmapFont getFont() {
        return font;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public void dispose() {
        super.dispose();
        batch.dispose();
        font.dispose();
        assetManager.dispose();
    }
}

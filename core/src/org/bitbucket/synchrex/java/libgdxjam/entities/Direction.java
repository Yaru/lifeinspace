package org.bitbucket.synchrex.java.libgdxjam.entities;

public enum Direction {
    LEFT,
    RIGHT,
    DOWN,
    UP,
    STOP
}

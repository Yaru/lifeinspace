package org.bitbucket.synchrex.java.libgdxjam.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Player extends Entity{

    public static final float MOD_SPEED = 1.5f;
    Sprite sprite;
    Texture texture;
    Direction dir;
    boolean visible;
    int health;
    private boolean active;

    public Player(float x, float y)
    {
        texture = new Texture(Gdx.files.internal("test.png"));
        sprite = new Sprite(texture);
        setPosition(x,y);
        setHealth(100);
        setVisible(true);
        setIsActive(true);
        dir = Direction.STOP;
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public void draw(Batch batch) {
        if (!getVisible()) {
            return;
        }
        sprite.draw(batch);
    }

    public void move(boolean faster) {
        if (!active) {
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            dir = Direction.RIGHT;
            if (faster) {
                translateX(SPEED * MOD_SPEED);
            }
            else {
                translateX(SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            dir = Direction.LEFT;
            if (faster) {
                translateX(-SPEED * MOD_SPEED);
            }
            else {
                translateX(-SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            dir = Direction.UP;
            if (faster) {
                translateY(SPEED * MOD_SPEED);
            }
            else {
                translateY(SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            dir = Direction.DOWN;
            if (faster) {
                translateY(-SPEED * MOD_SPEED);
            }
            else {
                translateY(-SPEED);
            }
        }
        switch (dir) {
            case LEFT:
                translateX(-DRIFT_SPEED);
                break;
            case RIGHT:
                translateX(DRIFT_SPEED);
                break;
            case DOWN:
                translateY(-DRIFT_SPEED);
                break;
            case UP:
                translateY(DRIFT_SPEED);
                break;
            case STOP:
                break;
        }
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void dispose() {
        texture.dispose();
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setIsActive(boolean isActive) {
        this.active = isActive;
    }

    public boolean isActive() {
        return active;
    }
}

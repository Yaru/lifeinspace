package org.bitbucket.synchrex.java.libgdxjam.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;

public class Ship extends Entity{

    public static final float MOD_SPEED = 1.7f;
    Sprite sprite;
    TextureAtlas atlas;
    int health;
    boolean canBeHurt;
    boolean visible;
    boolean isActive;
    float hurtTimer;
    Direction dir;

    public Ship(float X, float Y, AssetManager assetManager) {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("ship");
        sprite = new Sprite(region);
        setPosition(X,Y);
        setHealth(100);
        setVisible(true);
        setCanBeHurt(true);
        setIsActive(true);
        hurtTimer = 1;
        dir = Direction.STOP;
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void draw(Batch batch) {
        if (!isVisible()) {
            return;
        }
        sprite.draw(batch);
    }

    public void move(boolean faster) {
        rotate();
        if (Gdx.input.isKeyPressed(Keys.D)) {
            dir = Direction.RIGHT;
            if (faster) {
                translateX(SPEED * MOD_SPEED);
            }
            else {
                translateX(SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Keys.A)) {
            dir = Direction.LEFT;
            if (faster) {
                translateX(-SPEED * MOD_SPEED);
            }
            else {
                translateX(-SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Keys.W)) {
            dir = Direction.UP;
            if (faster) {
                translateY(SPEED * MOD_SPEED);
            }
            else {
                translateY(SPEED);
            }
        }
        else if (Gdx.input.isKeyPressed(Keys.S)) {
            dir = Direction.DOWN;
            if (faster) {
                translateY(-SPEED * MOD_SPEED);
            }
            else {
                translateY(-SPEED);
            }
        }
        if (!canBeHurt) {
            if (hurtTimer > 0) {
                sprite.setAlpha(0.5f);
                hurtTimer -= Gdx.graphics.getDeltaTime();
            }
            else {
                sprite.setAlpha(1);
                canBeHurt = true;
                hurtTimer = 1;
            }
        }
    }

    public float getWidth() {
        return sprite.getWidth();
    }

    public float getHeight() {
        return sprite.getHeight();
    }

    private void rotate() {
        if (Gdx.input.isKeyJustPressed(Keys.D) && getDir() != Direction.RIGHT) {
            sprite.setRotation(270);
        }
        else if (Gdx.input.isKeyJustPressed(Keys.A) && getDir() != Direction.LEFT) {
            sprite.setRotation(90);
        }
        else if (Gdx.input.isKeyJustPressed(Keys.W) && getDir() != Direction.UP) {
            sprite.setRotation(0);
        }
        else if (Gdx.input.isKeyJustPressed(Keys.S) && getDir() != Direction.DOWN) {
            sprite.setRotation(180);
        }
    }

    public int hurt(int damage)
    {
        health -= damage;
        return health;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isCanBeHurt() {
        return canBeHurt;
    }

    public void setCanBeHurt(boolean canBeHurt) {
        this.canBeHurt = canBeHurt;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Direction getDir() {
        return dir;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void dispose() {
        atlas.dispose();
    }
}

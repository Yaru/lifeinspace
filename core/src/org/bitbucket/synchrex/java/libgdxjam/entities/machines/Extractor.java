package org.bitbucket.synchrex.java.libgdxjam.entities.machines;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;

public class Extractor {

    Sprite sprite;
    TextureAtlas atlas;
    public boolean visible;
    public float extractionTimer;

    public Extractor(float X, float Y, AssetManager assetManager) {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("extractor");
        sprite = new Sprite(region);
        setPosition(X,Y);
        extractionTimer = -1;
        visible = true;
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public void draw(Batch batch) {
        if (visible) {
            sprite.draw(batch);
        }
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void dispose() {
        atlas.dispose();
    }
}

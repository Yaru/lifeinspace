package org.bitbucket.synchrex.java.libgdxjam.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;


public class Laser {

    Sprite sprite;
    TextureAtlas atlas;
    Direction direction;
    Boolean visible;
    int bulletSpeed = 8;
    float timer = 1.5f;

    public Laser(float X, float Y, Direction dir, AssetManager assetManager) {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("laser");
        sprite = new Sprite(region);
        setPosition(X,Y);
        direction = dir;
        switch (dir) {
            case DOWN:
            case UP:
                sprite.rotate(90);
                break;
        }
        visible = true;
    }

    public void draw(Batch batch) {
        if (!visible) {
            return;
        }
        switch (direction) {

            case LEFT:
                translateX(-bulletSpeed);
                break;
            case RIGHT:
                translateX(bulletSpeed);
                break;
            case DOWN:
                translateY(-bulletSpeed);
                break;
            case UP:
                translateY(bulletSpeed);
                break;
        }
        sprite.draw(batch);
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public Laser setTimer(float timer) {
        this.timer = timer;
        return this;
    }

    public Laser setVisible(boolean b) {
        visible = b;
        return this;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public float getTimer() {
        return timer;
    }

    public void tickTimer() {
        timer -= Gdx.graphics.getDeltaTime();
    }

    public void dispose() {
        atlas.dispose();
    }
}

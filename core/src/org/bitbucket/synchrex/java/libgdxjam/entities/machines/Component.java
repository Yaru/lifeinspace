package org.bitbucket.synchrex.java.libgdxjam.entities.machines;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import org.bitbucket.synchrex.java.libgdxjam.resources.Resource;
import org.bitbucket.synchrex.java.libgdxjam.resources.ResourceList;

public class Component {

    private String name;
    private Array<Resource> price;
    private int monetaryPrice;
    private String description;

    public Component() {
        price = new Array<Resource>();
        name = "NONAME";
        description = "NODESCRIPT";
        monetaryPrice = 0;
    }

    public String getName() {
        return name;
    }

    public Component setName(String name) {
        this.name = name;
        if (name == "FUEL TANK") {
            //price.add(ResourceList.getRandomResource().setQuantity(5));
            setMonetaryPrice(100);
        }
        else if (name == "O2 TANK") {
            //price.add(ResourceList.getRandomResource().setQuantity(10));
            setMonetaryPrice(300);
        }
        else if (name == "SPACEGATE INTERPRETER") {
            for (int i = 0; i < 4; i++) {
                Resource r = ResourceList.getRandomResource();
                price.add(ResourceList.getRandomResource().setQuantity(MathUtils.random(3, 8)));
            }
        }
        else {
            for (int i = 0; i < 2; i++) {
                Resource r = ResourceList.getRandomResource();
                price.add(ResourceList.getRandomResource().setQuantity(MathUtils.random(3, 8)));
            }
        }
        return this;
    }

    public Array<Resource> getPrice() {
        return price;
    }

    public Component setPrice(Array<Resource> price) {
        this.price = price;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Component setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getMonetaryPrice() {
        return monetaryPrice;
    }

    public Component setMonetaryPrice(int monetaryPrice) {
        this.monetaryPrice = monetaryPrice;
        return this;
    }
}

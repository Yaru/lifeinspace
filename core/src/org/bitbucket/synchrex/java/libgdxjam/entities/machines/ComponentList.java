package org.bitbucket.synchrex.java.libgdxjam.entities.machines;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class ComponentList {

    private static Array<Component> systemArray;

    public static void init()
    {
        systemArray  = new Array<Component>();
        systemArray.add(new Component().setName("NEUROPORT ABSORBER").setDescription("2x extraction speed.").setMonetaryPrice(200)); //DONE
        systemArray.add(new Component().setName("JETPACK").setDescription("Faster movement when outside the ship... somehow").setMonetaryPrice(400)); //DONE
        systemArray.add(new Component().setName("NEW MOTOR").setDescription("Faster ship movement").setMonetaryPrice(400)); //DONE
        systemArray.add(new Component().setName("NEUTRON ACTIVATOR").setDescription("Launch extractors from the ship.").setMonetaryPrice(600)); //DONE
        systemArray.add(new Component().setName("PHOTON OUTER").setDescription("Fuel expense halved").setMonetaryPrice(800)); //DONE
        systemArray.add(new Component().setName("SPACEGATE INTERPRETER").setDescription("Allows multiversal travel. And winning the game.").setMonetaryPrice(1200)); //DONE
        systemArray.add(new Component().setName("FUEL TANK").setDescription("Use with T.")); //DONE
        systemArray.add(new Component().setName("O2 TANK").setDescription("Use with T.")); //DONE
    }

    public static Component getRandomSystem()
    {
        return systemArray.get(MathUtils.random(systemArray.size - 1));
    }

    public static Array<Component> getList()
    {
        Array<Component> array = new Array<Component>();
        array.addAll(systemArray,0,systemArray.size - 2);
        return array;
    }

    public static Array<Component> getAllSystems()
    {
        return systemArray;
    }
}

package org.bitbucket.synchrex.java.libgdxjam.space;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import org.bitbucket.synchrex.java.libgdxjam.entities.Direction;
import org.bitbucket.synchrex.java.libgdxjam.utils.ColorUtil;

import java.awt.*;
import java.util.ArrayList;

public class EnemyShip {

    private static final float SPEED = 1.5f;
    private final ArrayList<Planet> planets;
    Sprite sprite;
    TextureAtlas atlas;
    int health;
    boolean canBeHurt;
    float hurtTimer;
    Direction dir;
    boolean extracting;
    boolean visible;

    float objectiveTimer;
    float extractionTimer;
    private Planet target;

    public EnemyShip(float X, float Y, AssetManager assetManager,ArrayList<Planet> planetArray) {
        planets = planetArray;
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("ship"); //TODO: Make a unique enemy sprite.
        sprite = new Sprite(region);
        setPosition(X,Y);
        Color color = ColorUtil.getRandomColor();
        sprite.setColor(color.getRed(),color.getGreen(),color.getBlue(),1);
        objectiveTimer = 0;
        extracting = false;
        extractionTimer = -10;
        canBeHurt = true;
        hurtTimer = 0.1f;
        setHealth(10);
        setVisible(true);
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void draw(Batch batch) {
        if (!visible) {
            return;
        }
        sprite.draw(batch);
        if (objectiveTimer > 0 && !isExtracting()) {
            objectiveTimer -= Gdx.graphics.getDeltaTime();
        }
        else {
            int rand = MathUtils.random(0,planets.size() - 1);
            target = planets.get(rand);
            objectiveTimer = 35;
        }
        if (target != null && !isExtracting()) {
            move(target);
        }

        if (hurtTimer > 0) {
            hurtTimer -= Gdx.graphics.getDeltaTime();
        }
        else {
            canBeHurt = true;
            hurtTimer = 0.1f;
        }

    }

    private void move(Planet target) {
        if (extracting) {
            return;
        }
        if (getX() < target.getX()) {
            translateX(SPEED);
        }
        else if (getX() > target.getX()) {
            translateX(-SPEED);
        }

        if (getY() < target.getY()) {
            translateY(SPEED);
        }
        else if (getY() > target.getY()) {
            translateY(-SPEED);
        }

    }

    public float getWidth() {
        return sprite.getWidth();
    }

    public float getHeight() {
        return sprite.getHeight();
    }

    public int hurt(int damage) {
        if (!canBeHurt) {
            return -1;
        }
        health -= damage;
        canBeHurt = false;
        return health;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isCanBeHurt() {
        return canBeHurt;
    }

    public void setCanBeHurt(boolean canBeHurt) {
        this.canBeHurt = canBeHurt;
    }

    public Direction getDir() {
        return dir;
    }

    public boolean isExtracting() {
        return extracting;
    }

    public void setExtracting(boolean extracting) {
        this.extracting = extracting;
    }

    public void setObjectiveTimer(float objectiveTimer) {
        this.objectiveTimer = objectiveTimer;
    }

    public float getExtractionTimer() {
        return extractionTimer;
    }

    public void setExtractionTimer(float extractionTimer) {
        this.extractionTimer = extractionTimer;
    }

    public Planet getTarget() {
        return target;
    }

    public void setTarget(Planet target) {
        this.target = target;
    }

    public void dispose() {
        atlas.dispose();
    }

    public void tickTimer() {
        extractionTimer -= Gdx.graphics.getDeltaTime();
    }

    public void changeTarget() {
        int rand = MathUtils.random(0,planets.size() - 1);
        /*if (planets.get(rand).isBeingExtracted() || planets.get(rand).getResources().size == 0) {
            changeTarget();
        }*/
        /*else {
            target = planets.get(rand);
            objectiveTimer = 25;
        }*/
        target = planets.get(rand);
        objectiveTimer = 25;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}

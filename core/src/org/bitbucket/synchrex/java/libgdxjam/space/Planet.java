package org.bitbucket.synchrex.java.libgdxjam.space;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import org.bitbucket.synchrex.java.libgdxjam.resources.Resource;
import org.bitbucket.synchrex.java.libgdxjam.screens.MainScreen;
import org.bitbucket.synchrex.java.libgdxjam.utils.ColorUtil;

import java.awt.*;
import java.util.Random;

public class Planet {

    Sprite sprite;
    TextureAtlas atlas;
    Array<Resource> resources;
    MainScreen world;
    Boolean beingExtracted;
    float respawnTimer = -1;

    public Planet(float X, float Y, MainScreen World, AssetManager assetManager)
    {
        world = World;
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("planet_base");
        sprite = new Sprite(region);
        Color color = ColorUtil.getRandomColor();
        sprite.setColor(color.getRed(),color.getGreen(),color.getBlue(),1);
        setPosition(X, Y);
        resources = new Array<Resource>();
        setBeingExtracted(false);
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public void draw(Batch batch) {
        sprite.draw(batch);
        setBeingExtracted(false);
        if (resources.size == 0) {
            if (respawnTimer == -1)
            {
                respawnTimer = 30;
            }
            if (respawnTimer > 0)
            {
                respawnTimer -= Gdx.graphics.getDeltaTime();
            }
            else {
                world.addResourcesToPlanet(this);
                respawnTimer = -1;
            }
        }
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void dispose() {
        atlas.dispose();
    }

    public Planet addResource(Resource r)
    {
        resources.add(r);
        return this;
    }

    public void removeResource (Resource r)
    {
        resources.removeIndex(resources.indexOf(r,true));
    }

    public Array<Resource> getResources()
    {
        return resources;
    }

    public Boolean isBeingExtracted() {
        return beingExtracted;
    }

    public void setBeingExtracted(Boolean beingExtracted) {
        this.beingExtracted = beingExtracted;
    }
}

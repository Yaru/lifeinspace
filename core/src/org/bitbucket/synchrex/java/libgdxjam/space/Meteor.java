package org.bitbucket.synchrex.java.libgdxjam.space;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Pc on 26/12/2015.
 */
public class Meteor {

    Sprite sprite;
    TextureAtlas atlas;
    int direction;
    boolean visible,canHurt;
    AtlasRegion region;
    int life;
    float hurtTimer;

    public Meteor(float X, float Y,AssetManager assetManager) {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        String[]meteors = new String[]{"tiny_meteor","medium_meteor"};
        region = atlas.findRegion(meteors[MathUtils.random(0,1)]);
        switch (region.name) {
            case "tiny_meteor":
                life = 3;
                break;
            case "medium_meteor":
                life = 7;
                break;
        }
        sprite = new Sprite(region);
        direction = MathUtils.random(0,3);
        visible = true;
        canHurt = true;
        hurtTimer = 0.1f;
        setPosition(X,Y);
    }

    public void draw(Batch batch) {
        if (!visible) {
            return;
        }
        sprite.draw(batch);
        switch (direction){
            case 0:
                translateX(-2f);
                break;
            case 1:
                translateX(2f);
                break;
            case 2:
                translateY(-2f);
                break;
            case 3:
                translateY(2f);
                break;
        }
        if (getX() < -2000 || getX() > 2000 || getY() < -2000 || getY() > 2000) {
            setVisible(false);
        }
        if (!canHurt) {
            if (hurtTimer > 0) {
                hurtTimer -= Gdx.graphics.getDeltaTime();
            }
            else {
                hurtTimer = 0.1f;
                canHurt = true;
            }
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public String getMeteorType()
    {
        return region.name;
    }

    public int getReward()
    {
        switch (getMeteorType()) {
            case "tiny_meteor":
                return 3;
            case "medium_meteor":
                return 6;
        }
        return -1;
    }

    public int getLife() {
        return life;
    }

    public void hurt()
    {
        if (!canHurt) {
            return;
        }
        life --;
        if (life <= 0) {
            setVisible(false);
        }
        canHurt = false;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void dispose() {
        atlas.dispose();
    }
}

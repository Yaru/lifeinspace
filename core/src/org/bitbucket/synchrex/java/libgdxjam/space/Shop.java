package org.bitbucket.synchrex.java.libgdxjam.space;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import org.bitbucket.synchrex.java.libgdxjam.resources.Resource;

public class Shop {

    Sprite sprite;
    TextureAtlas atlas;
    Array<Resource> resources;

    public Shop(float X, float Y, AssetManager assetManager)
    {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("planet_base");
        sprite = new Sprite(region);
        sprite.setColor(Color.RED);
        setPosition(X, Y);
        resources = new Array<Resource>();
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public void draw(Batch batch) {
        sprite.draw(batch);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public float getWidth() {
        return sprite.getWidth();
    }

    public float getHeight() {
        return sprite.getHeight();
    }

    public void dispose() {
        atlas.dispose();
    }

    public Shop addResource(Resource r,int quantity)
    {
        Boolean i = false;
        for (Resource re:resources) {
            if (re.getName() == r.getName()) {
                re.setQuantity(re.getQuantity() + quantity);
                i = true;
                return this;
            }
        }
        if (!i) {
            resources.add(r);
            return this;
        }
        return this;
    }

    public Resource removeResource (String resourceName)
    {
        for (int i = 0; i < resources.size; i++) {
            if (resources.get(i).getName() == resourceName)
            {
                Resource r = resources.get(i);
                if (r.getQuantity() > 1) {
                    r.setQuantity(r.getQuantity() - 1);
                }
                else {
                    resources.removeIndex(i);
                }
                return r;
            }
        }
        return null;
    }

    public Array<Resource> getResources()
    {
        return resources;
    }
}

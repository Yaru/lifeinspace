package org.bitbucket.synchrex.java.libgdxjam.space;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import org.bitbucket.synchrex.java.libgdxjam.resources.Resource;
import org.bitbucket.synchrex.java.libgdxjam.screens.MainScreen;
import org.bitbucket.synchrex.java.libgdxjam.utils.ColorUtil;

import java.awt.*;

public class SpaceGate {

    Sprite sprite;
    TextureAtlas atlas;

    public SpaceGate(float X, float Y, MainScreen World, AssetManager assetManager)
    {
        atlas = assetManager.get("texture.txt",TextureAtlas.class);
        AtlasRegion region = atlas.findRegion("spacegate");
        sprite = new Sprite(region);
        setPosition(X, Y);
    }

    public Rectangle getBoundingRectangle() {
        return sprite.getBoundingRectangle();
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void translateX(float xAmount) {
        sprite.translateX(xAmount);
    }

    public void translateY(float yAmount) {
        sprite.translateY(yAmount);
    }

    public void draw(Batch batch) {
        sprite.draw(batch);
    }

    public float getX() {
        return sprite.getX();
    }

    public float getY() {
        return sprite.getY();
    }

    public void dispose() {
        atlas.dispose();
    }
}

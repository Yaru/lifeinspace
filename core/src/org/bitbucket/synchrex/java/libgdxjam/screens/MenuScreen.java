package org.bitbucket.synchrex.java.libgdxjam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import org.bitbucket.synchrex.java.libgdxjam.LibGDXJam;

/**
 * Created by Pc on 11/01/2016.
 */
public class MenuScreen implements Screen{

    private final LibGDXJam game;
    private String[] options;
    private int optionIndex;

    public MenuScreen(LibGDXJam Game)
    {
        game = Game;
        options = new String[]{"> START < \n\nHOW TO PLAY\n\nCREDITS","START \n\n> HOW TO PLAY <\n\nCREDITS","START \n\nHOW TO PLAY\n\n> CREDITS <"};
        optionIndex = 0;
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.getBatch().begin();
        game.getFont().draw(game.getBatch(),options[optionIndex],Gdx.graphics.getWidth() / 2 - 60, Gdx.graphics.getHeight() / 2 + 40);
        game.getFont().draw(game.getBatch(),"ARROW KEYS OR WASD TO SELECT, SPACE TO CONFIRM",Gdx.graphics.getWidth() / 2 - 200,20);
        game.getBatch().end();

        if (Gdx.input.isKeyJustPressed(Keys.DOWN) || Gdx.input.isKeyJustPressed(Keys.S)) {
            if (optionIndex < 2) {
                optionIndex ++;
            }
            else {
                optionIndex = 0;
            }
        }

        if (Gdx.input.isKeyJustPressed(Keys.UP) || Gdx.input.isKeyJustPressed(Keys.W)) {
            if (optionIndex > 0) {
                optionIndex --;
            }
            else {
                optionIndex = 2;
            }
        }

        if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
            switch (optionIndex) {
                case 0:
                    game.setScreen(new MainScreen(game));
                    break;
                case 1:
                    game.setScreen(new HowToScreen(game));
                    break;
                case 2:
                    game.setScreen(new CreditsScreen(game));
                    break;
            }
        }

    }
    public void show() {
    }
    public void resize(int width, int height) {
    }
    public void pause() {
    }
    public void resume() {
    }
    public void hide() {
    }
    public void dispose() {
    }
}

package org.bitbucket.synchrex.java.libgdxjam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import org.bitbucket.synchrex.java.libgdxjam.LibGDXJam;


public class HowToScreen implements Screen {

    private final LibGDXJam game;

    public HowToScreen(LibGDXJam game) {
        this.game = game;
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.getBatch().begin();
        game.getFont().draw(game.getBatch(), "MOVE YOUR SHIP WITH WASD.\n" +
                "SCAN THE PLANETS PRESSING 1.\n" +
                "ABANDON THE SHIP PRESSING ENTER\n" +
                "FIRE THE LASERS WITH CTRL\n" +
                "IF YOU'RE OUTSIDE THE SHIP, YOU CAN EXTRACT MINERALS FROM THE PLANETS\n" +
                "PRESSING 2\n" +
                "PRESS T TO USE FUEL TANKS\n" +
                "PRESS R TO SEE YOUR RESOURCE LIST\n" +
                "INTERACT WITH THE SHOP PRESSING SPACE\n" +
                "PLANETS SPAWN NEW RESOURCES AFTER SOME TIME\n" +
                "BUY THE SPACEGATE INTERPRETER AND FIND THE SPACE GATE TO WIN.", 10, Gdx.graphics.getHeight() - 20);
        game.getFont().draw(game.getBatch(), "Press any key...", 20, 20);
        game.getBatch().end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
            game.setScreen(new MenuScreen(game));
        }

    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

package org.bitbucket.synchrex.java.libgdxjam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import org.bitbucket.synchrex.java.libgdxjam.LibGDXJam;

public class CreditsScreen implements Screen {

    private final LibGDXJam game;

    public CreditsScreen(LibGDXJam game)
    {
        this.game = game;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.getBatch().begin();
        game.getFont().draw(game.getBatch(),"(Bad)Code: Yaru Katsaros\n" +
                "(Awful) Planet graphics: Yaru Katsaros\n" +
                        "(Awesome) Ship, Meteor and Extractor graphics: Pudman\n" +
                        "(http://opengameart.org/users/pudman)",
                Gdx.graphics.getWidth() / 2 - 120, Gdx.graphics.getHeight() / 2 + 60);
        game.getFont().draw(game.getBatch(), "Press any key...", 20, 20);
        game.getBatch().end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
            game.setScreen(new MenuScreen(game));
        }
    }

    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

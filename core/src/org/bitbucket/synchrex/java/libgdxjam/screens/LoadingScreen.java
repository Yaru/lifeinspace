package org.bitbucket.synchrex.java.libgdxjam.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import org.bitbucket.synchrex.java.libgdxjam.LibGDXJam;

public class LoadingScreen implements Screen {

    private final LibGDXJam game;

    public LoadingScreen(LibGDXJam game) {
        this.game = game;
        game.getAssetManager().load("texture.txt",TextureAtlas.class);
    }

    public void show() {

    }

    public void render(float delta) {
        if (game.getAssetManager().update()) {
            game.setScreen(new MenuScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

package org.bitbucket.synchrex.java.libgdxjam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Array;
import org.bitbucket.synchrex.java.libgdxjam.LibGDXJam;
import org.bitbucket.synchrex.java.libgdxjam.entities.Direction;
import org.bitbucket.synchrex.java.libgdxjam.entities.Laser;
import org.bitbucket.synchrex.java.libgdxjam.entities.machines.Component;
import org.bitbucket.synchrex.java.libgdxjam.entities.machines.Extractor;
import org.bitbucket.synchrex.java.libgdxjam.entities.Player;
import org.bitbucket.synchrex.java.libgdxjam.entities.Ship;
import org.bitbucket.synchrex.java.libgdxjam.entities.machines.ComponentList;
import org.bitbucket.synchrex.java.libgdxjam.resources.Resource;
import org.bitbucket.synchrex.java.libgdxjam.resources.ResourceList;
import org.bitbucket.synchrex.java.libgdxjam.resources.ResourceOnMap;
import org.bitbucket.synchrex.java.libgdxjam.space.*;

import java.util.ArrayList;

public class MainScreen implements Screen {

    private LibGDXJam game;
    private static final float OXYGEN_TIMER_VALUE = 2;
    private static final float FUEL_TIMER_VALUE = 5;
    private static final float METEOR_TIMER_VALUE = 8;
    private static final float ENEMY_TIMER_VALUE = 60;
    private OrthographicCamera camera;
    private SpriteBatch batch,hudBatch;
    private BitmapFont font;
    private Sprite fade;
    private SpaceGate gate;
    float fadeAlpha;
    private boolean gameWon;

    //DEBUG
    //ShapeRenderer renderer;

    private Player player;
    private Ship ship;
    private Shop shop;
    ArrayList<Planet> planets;
    Array<Resource> resources;
    Array<ResourceOnMap> resourceContainer;
    Array<Meteor> meteors;
    Array<Extractor> extractors;
    Array<Laser> lasers;
    Array<EnemyShip> enemies;
    Boolean drawResourceScreen,drawComponentsScreen,drawShopScreen,drawBuyScreen,drawSellScreen;
    float meteorTimer;
    float enemyTimer;
    Boolean onShip;
    float oxygenTimer;
    float fuelTimer;
    int oxygen;
    int fuel;
    int fuelTanks;
    int oxygenTanks;
    int money;
    int maxExtractors;
    int selectorY;
    int selectedOption;

    Array<Component> components;
    Array<Component> componentList;

    public MainScreen(LibGDXJam game)
    {
        this.game = game;
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(w,h);
        camera.update();
        batch = game.getBatch();
        hudBatch = new SpriteBatch();
        font = game.getFont();
        init(game);

    }

    private void init(LibGDXJam game) {
        ship = new Ship(50,50,game.getAssetManager());
        player = new Player(2000,2000);
        oxygen = 100;
        fuel = 100;
        fuelTanks = 2;
        oxygenTanks = 2;
        oxygenTimer = OXYGEN_TIMER_VALUE;
        fuelTimer = FUEL_TIMER_VALUE;
        meteorTimer = METEOR_TIMER_VALUE;
        enemyTimer = ENEMY_TIMER_VALUE;
        money = 1000;
        maxExtractors = 3;
        onShip = true;
        camera.position.set(50,50,0);
        planets = new ArrayList<Planet>();
        ResourceList.init();
        ComponentList.init();
        componentList = ComponentList.getList();
        for (int i = 0; i < 18; i++) {
            float X = MathUtils.random(-900, 900);
            float Y = MathUtils.random(-900,900);
            Planet planet = new Planet(X,Y,this,game.getAssetManager());
            addResourcesToPlanet(planet);
            planets.add(planet);
        }
        shop = new Shop(ship.getX(), ship.getY(),game.getAssetManager());
        resources = new Array<Resource>();
        drawResourceScreen = drawComponentsScreen = drawShopScreen = drawBuyScreen = drawSellScreen = false;
        extractors = new Array<Extractor>();
        components = new Array<Component>();
        meteors = new Array<Meteor>();
        lasers = new Array<Laser>();
        enemies = new Array<EnemyShip>();
        resourceContainer = new Array<ResourceOnMap>();
        fade = new Sprite(new Texture(Gdx.files.internal("fade.png")));
        gameWon = false;
        fadeAlpha = 0;
        float X = MathUtils.random(-900, 900);
        float Y = MathUtils.random(-900,900);
        gate = new SpaceGate(X,Y,this,game.getAssetManager());
        //renderer = new ShapeRenderer(); //DEBUG
    }

    public void addResourcesToPlanet(Planet p)
    {
        for (int j = 0; j < MathUtils.random(1,8); j++) {
            Resource r = ResourceList.getRandomResource();
            if (MathUtils.randomBoolean(r.getRarity())) {
                p.addResource(ResourceList.getRandomResource());
            }
        }
    }

    public void show() {
    }

    public void render(float delta) {
        //debugFunctions();
        if (Gdx.input.isKeyJustPressed(Keys.ENTER) && ship.isActive() && ship.getHealth() > 0) {
            if (onShip) {
                player.setPosition(ship.getX() + 20,ship.getY());
                onShip = false;
            }
            else {
                if (Vector2.dst(player.getX(),player.getY(),ship.getX(),ship.getY()) <= 50) {
                    player.setPosition(2000,2000);
                    onShip = true;
                }
            }
        }
        float lerp = 0.1f;
        Vector3 position = camera.position;
        if (onShip) {
            position.x += (ship.getX() - position.x) * lerp;
            position.y += (ship.getY() - position.y) * lerp;
        }
        else {
            position.x += (player.getX() - position.x) * lerp;
            position.y += (player.getY() - position.y) * lerp;
        }
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        /*renderer.setProjectionMatrix(camera.combined);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(Color.RED);
        renderer.line(-1000,-1000,0,1000,-1000,0);
        renderer.line(-1000,1000,0,1000,1000,0);
        renderer.line(-1000,1000,0,-1000,-1000,0);
        renderer.line(1000,1000,0,1000,-1000,0);
        renderer.end();*/
        batch.begin();
        if (!onShip) {
            player.draw(batch);
            player.move(checkForComponent("JETPACK"));
            if (Gdx.input.isKeyJustPressed(Keys.T) && oxygenTanks > 0) {
                oxygenTanks--;
                oxygen += 50;
                if (oxygen > 100) {
                    oxygen = 100;
                }
            }
        }
        else {
            if (ship.isActive()) {
                if (fuel > 0) {
                    ship.move(checkForComponent("NEW MOTOR"));
                }
                if (Gdx.input.isKeyJustPressed(Keys.T) && fuelTanks > 0) {
                    fuelTanks--;
                    fuel += 50;
                    if (fuel > 100) {
                        fuel = 100;
                    }
                }
            }
        }
        if (!drawBuyScreen && !drawSellScreen && !drawShopScreen) {
            font.draw(batch,"SHOP",shop.getX() + shop.getWidth() / 2 - 15,shop.getY() + shop.getHeight() + 20);
        }
        drawPlanets();
        shop.draw(batch);
        for (Laser laser:lasers) {
            laser.draw(batch);
            if (lasers.size >= 20) {
                lasers.removeValue(lasers.first(), true);
            }
            if (laser.getTimer() > 0) {
                laser.tickTimer();
            }
            else {
                lasers.removeValue(laser,true);
            }
            for (EnemyShip es:enemies) {
                if (Intersector.overlaps(laser.getBoundingRectangle(),es.getBoundingRectangle()) && laser.isVisible()) {
                    laser.setVisible(false);
                    System.out.println("OUCH");
                    if (es.getHealth() <= 0) {
                        enemies.removeValue(es, true);
                    }
                    lasers.removeValue(laser,true);
                    es.hurt(1);
                    break;
                }
            }
        }
        if (Intersector.overlaps(ship.getBoundingRectangle(),shop.getBoundingRectangle()) || Intersector.overlaps(player.getBoundingRectangle(),shop.getBoundingRectangle())) {
            if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
                drawBuyScreen = drawSellScreen = drawComponentsScreen = drawResourceScreen = false;
                drawShopScreen = !drawShopScreen;
                if (!ship.isActive()) {
                    ship.setIsActive(true);
                }
                if (!player.isActive()) {
                    player.setIsActive(true);
                }
            }
        }
        ship.draw(batch);
        drawMeteors();
        drawEnemies();
        if (checkForComponent("SPACEGATE INTERPRETER")) {
            gate.draw(batch);
            if (Intersector.overlaps(ship.getBoundingRectangle(), gate.getBoundingRectangle()) && !gameWon) {
                gameWon = true;
                ship.setIsActive(false);
            }
        }
        batch.end();
        drawScreens();
        if (!onShip) {
            if (oxygenTimer > 0) {
                oxygenTimer -= Gdx.graphics.getDeltaTime();
            }
            else {
                if (player.isActive()) {
                    oxygen -= 5;
                    oxygenTimer = OXYGEN_TIMER_VALUE;
                    if (oxygen <= 10) {
                        font.setColor(Color.RED);
                    }
                    if (oxygen <= 0) {
                        oxygen = 0;
                    }
                }
            }
            hudBatch.begin();
            font.draw(hudBatch,"O2: " + oxygen + "%",20,20);
            font.setColor(Color.WHITE);
            font.draw(hudBatch,"[T]TANKS: " + oxygenTanks,Gdx.graphics.getWidth() - 100,20);
            if (!drawComponentsScreen && !drawResourceScreen && !drawShopScreen && !drawBuyScreen && !drawSellScreen) {
                font.draw(hudBatch,"[2] EXTRACTORS: " + extractors.size + "/" + maxExtractors,20,Gdx.graphics.getHeight() - 20);
            }
        }
        else {
            if (fuelTimer > 0) {
                fuelTimer -= Gdx.graphics.getDeltaTime();
            }
            else {
                if (ship.isActive()) {
                    fuel -= 5;
                    if (oxygen < 100) {
                        oxygen += 10;
                    }
                }
                if (checkForComponent("PHOTON OUTER")) {
                    fuelTimer = FUEL_TIMER_VALUE * 2;
                }
                else {
                    fuelTimer = FUEL_TIMER_VALUE;
                }
                if (fuel <= 0) {
                    fuel = 0;
                }
            }
            hudBatch.begin();
            font.draw(hudBatch, "FUEL: " + fuel + "%       HULL: " + ship.getHealth() + "%", 20, 20);
            font.draw(hudBatch, "[T]TANKS: " + fuelTanks, Gdx.graphics.getWidth() - 100, 20);
        }
        if (ship.getHealth() <= 0 || player.getHealth() <= 0 || oxygen <= 0) {
            ship.setVisible(false);
            ship.setIsActive(false);
            player.setVisible(false);
            player.setIsActive(false);
            font.draw(hudBatch, "YOU DIED. PRESS ENTER...", Gdx.graphics.getWidth() / 2 - 90, Gdx.graphics.getHeight() / 2);
            if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
                restartGame();
            }
        }
        font.draw(hudBatch,"CREDITS: " + money,20,40);
        if (gameWon) {
            ship.setIsActive(false);
            player.setIsActive(false);
            if (fadeAlpha < 0.9) {
                fadeAlpha += Gdx.graphics.getDeltaTime();
            }
            fade.setAlpha(fadeAlpha);
            fade.draw(hudBatch);
            if (fadeAlpha >= 0.9) {
                font.setColor(Color.BLACK);
                font.draw(hudBatch, "THANKS FOR PLAYING", Gdx.graphics.getWidth() / 2 - 70, Gdx.graphics.getHeight() / 2 + 60);
                font.draw(hudBatch, "Press enter...", Gdx.graphics.getWidth() / 2 - 40, Gdx.graphics.getHeight() / 2 + 30);
                font.setColor(Color.WHITE);
                if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
                    restartGame();
                }
            }
        }
        if (checkForComponent("SPACEGATE INTERPRETER")) {
            font.draw(hudBatch,"SPACEGATE OPEN",30,60);
        }
        hudBatch.end();
        checkPosition();
    }

    private void restartGame() {
        System.out.println("Restart!");
        init(game);
    }

    private void drawPlanets() {
        for (int i = 0; i < planets.size(); i++) {
            Planet planet = planets.get(i);
            for(Planet planetCheck:planets) {
                if (!planet.equals(planetCheck) && Intersector.overlaps(planet.getBoundingRectangle(), planetCheck.getBoundingRectangle()) || Intersector.overlaps(planet.getBoundingRectangle(),shop.getBoundingRectangle())) {
                    float X = MathUtils.random(-900, 900);
                    float Y = MathUtils.random(-900,900);
                    planet.setPosition(X,Y);
                }
            }
            if (Vector2.dst(ship.getX(), ship.getY(), planet.getX(), planet.getY()) < 600 || Vector2.dst(player.getX(),player.getY(),planet.getX(),planet.getY()) <= 600) {
                if (onShip) {
                    handleShipFunctions(planet);
                }
                else {
                    handleAstronautFunctions(planet);
                }
                planet.draw(batch);

            }
            if (extractors.size != 0) {
                for(Extractor e:extractors) {
                    e.draw(batch);
                    if (Intersector.overlaps(e.getBoundingRectangle(),planet.getBoundingRectangle())) {
                        extractMineral(planet,e);
                        planet.setBeingExtracted(true);
                    }
                }
            }

            for (EnemyShip es:enemies) {
                if (Intersector.overlaps(es.getBoundingRectangle(),planet.getBoundingRectangle())) {
                    es.setExtracting(true);
                    planet.setBeingExtracted(true);
                    enemyExtracting(planet,es);
                }
            }
        }
    }

    private void drawMeteors()
    {
        if (meteorTimer > 0) {
            meteorTimer -= Gdx.graphics.getDeltaTime();
        }
        else {
            float X = MathUtils.random(-900,900);
            float Y = MathUtils.random(-900,900);
            meteors.add(new Meteor(X,Y,game.getAssetManager()));
            meteorTimer = METEOR_TIMER_VALUE;
        }
        for (Meteor m:meteors) {
            m.draw(batch);
            if (onShip) {
                if (Intersector.overlaps(m.getBoundingRectangle(),ship.getBoundingRectangle()) && ship.isCanBeHurt() && ship.isActive()) {
                    switch (m.getMeteorType()) {
                        case "tiny_meteor":
                            ship.hurt(5);
                            break;
                        case "medium_meteor":
                            ship.hurt(10);
                            break;
                    }
                    ship.setCanBeHurt(false);
                }
            }
            else {
                if (Intersector.overlaps(m.getBoundingRectangle(),player.getBoundingRectangle()) && player.isActive()) {
                    player.setHealth(0);
                }
            }

            for(ResourceOnMap rom:resourceContainer) {
                rom.draw(batch);
                if (Intersector.overlaps(ship.getBoundingRectangle(),rom.getBoundingRectangle()) || Intersector.overlaps(player.getBoundingRectangle(),rom.getBoundingRectangle())) {
                    Resource r = ResourceList.getRandomResource();
                    grabResource(r);
                    resourceContainer.removeValue(rom,true);
                }
            }
            int bob = 0;
            for (Laser l:lasers) {
                if (Intersector.overlaps(l.getBoundingRectangle(),m.getBoundingRectangle())) {
                    lasers.removeValue(l,false);
                    System.out.println("VISIBLE:" + l.isVisible());
                    m.hurt();
                    if(m.getLife() <= 0) {
                        for (int i = 0; i < m.getReward(); i++) {
                            float xx = MathUtils.random(m.getX() - 100, m.getX() + 100);
                            float yy = MathUtils.random(m.getY() - 100, m.getY() + 100);
                            resourceContainer.add(new ResourceOnMap(xx,yy,game.getAssetManager()));
                        }
                        meteors.removeValue(m,true);
                        break;
                    }
                    bob ++;
                    System.out.println(bob);
                    break;
                }
            }
        }
    }

    private void drawEnemies()
    {
        if (enemyTimer > 0) {
            enemyTimer -= Gdx.graphics.getDeltaTime();
        }
        else {
            float X = MathUtils.random(-900,900);
            float Y = MathUtils.random(-900,900);
            enemies.add(new EnemyShip(X,Y,game.getAssetManager(),planets));
            enemyTimer = ENEMY_TIMER_VALUE;
        }

        for (EnemyShip es:enemies) {
            es.draw(batch);
        }

    }

    private void debugFunctions() {
        //DEBUG
        if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyJustPressed(Keys.F12)) {
            restartGame();
        }

        if (Gdx.input.isKeyJustPressed(Keys.F5)) {
            ship.setHealth(0);
        }

        if (Gdx.input.isKeyJustPressed(Keys.F6)) {
            gameWon = true;
        }

        if (Gdx.input.isKeyJustPressed(Keys.F9)) {
            enemies.add(new EnemyShip(ship.getX(),ship.getY(),game.getAssetManager(),planets));
        }

        if (Gdx.input.isKeyJustPressed(Keys.F10)) {
            meteors.add(new Meteor(ship.getX(),ship.getY(),game.getAssetManager()));
        }

        if (Gdx.input.isKeyJustPressed(Keys.F11)) {
            resources.clear();
            for (Resource re:ResourceList.getAllResources())
            {
                resources.add(re);
                for (Resource r:resources)
                {
                    r.setQuantity(99);
                }
            }
            components.clear();
            components.add(new Component().setName("TEST"));
        }
    }

    private void checkPosition() {
        if (onShip) {
            if (ship.getX() >= 1100) {
                ship.setPosition(-1100,ship.getY());
                camera.position.set(-1100,ship.getY(),0);
            }
            else if (ship.getX() <= -1100) {
                ship.setPosition(1100,ship.getY());
                camera.position.set(1100,ship.getY(),0);
            }
            if (ship.getY() <= -1100) {
                ship.setPosition(ship.getX(), 1100);
                camera.position.set(ship.getX(), ship.getY(), 0);
            }
            else if (ship.getY() >= 1100) {
                ship.setPosition(ship.getX(),-1100);
                camera.position.set(ship.getX(),ship.getY(),0);
            }
        }
        else {
            if (player.getX() >= 1100) {
                player.setPosition(-1100,player.getY());
                camera.position.set(-1100,player.getY(),0);
            }
            else if (player.getX() <= -1100) {
                player.setPosition(1100,player.getY());
                camera.position.set(1100,player.getY(),0);
            }
            if (player.getY() <= -1100) {
                player.setPosition(player.getX(), 1100);
                camera.position.set(player.getX(), player.getY(), 0);
            }
            else if (player.getY() >= 1100) {
                player.setPosition(player.getX(),-1100);
                camera.position.set(player.getX(),player.getY(),0);
            }
        }
    }

    private void drawScreens() {
        hudBatch.begin();
        if (Gdx.input.isKeyJustPressed(Keys.R) && ship.isActive()) {
            drawShopScreen = drawBuyScreen = drawSellScreen = false;
            drawResourceScreen = !drawResourceScreen;
            drawComponentsScreen = false;
        }
        if (Gdx.input.isKeyJustPressed(Keys.C) && ship.isActive()) {
            drawShopScreen = drawBuyScreen = drawSellScreen = false;
            drawComponentsScreen = !drawComponentsScreen;
            drawResourceScreen = false;
        }
        if (drawResourceScreen) {
            String s = "-RESOURCES-\n----------------------\n";
            for (Resource r:resources)
            {
                s += r.getName() + " [" + r.getQuantity() + "]\n";
            }
            font.draw(hudBatch,s,20,Gdx.graphics.getHeight() - 20);
        }
        if (drawComponentsScreen) {
            String s = "-COMPONENTS-\n---------------------\n";
            int y = 60;
            for (int i = 0; i < componentList.size; i++) {
                for (Component c:components)
                {
                    if (c.getName() == componentList.get(i).getName()) {
                        font.setColor(Color.GREEN);
                        break;
                    }
                    else {
                        font.setColor(Color.RED);
                    }
                }
                font.draw(hudBatch, componentList.get(i).getName(), 20, Gdx.graphics.getHeight() - y);
                y += 20;
            }
            font.setColor(Color.WHITE);
            font.draw(hudBatch, s, 20, Gdx.graphics.getHeight() - 20);
        }
        if (drawShopScreen) {
            drawComponentsScreen = drawResourceScreen = false;
            if (ship.isActive()) {
                ship.setIsActive(false);
            }
            if (player.isActive()) {
                player.setIsActive(false);
            }
            font.draw(hudBatch, "SHOP -- [F1] BUY [F2] SELL [SPACE] CLOSE", 20, Gdx.graphics.getHeight() - 20);
            if (Gdx.input.isKeyJustPressed(Keys.F1)) {
                drawShopScreen = false;
                drawBuyScreen = !drawBuyScreen;
                selectorY = Gdx.graphics.getHeight() - 47;
                selectedOption = 0;

            }
            if (Gdx.input.isKeyJustPressed(Keys.F2) && resources.size != 0) {
                drawShopScreen = false;
                selectorY = Gdx.graphics.getHeight() - 40;
                selectedOption = 0;
                drawSellScreen = !drawSellScreen;
            }

        }
        if (drawBuyScreen) {
            handleBuyScreen();
        }
        if (drawSellScreen) {
            int y = Gdx.graphics.getHeight() - 50;
            String s = "SELL -- [ARROW KEYS] MOVE [ENTER] SELL [SPACE] CLOSE\n";
            for (Resource r:resources)
            {
                font.draw(hudBatch,r.getName() + " [" + r.getQuantity() + "] -- " + r.getMonetaryValue() + "\n\n",20,y);
                y -= 30;
            }
            font.draw(hudBatch,s, 20, Gdx.graphics.getHeight() - 20);
            font.draw(hudBatch,"<",210,selectorY);
            if (Gdx.input.isKeyJustPressed(Keys.DOWN)) {
                if (selectedOption < resources.size - 1) {
                    selectedOption++;
                    selectorY -= 30;
                }
                else {
                    selectedOption = 0;
                    selectorY = Gdx.graphics.getHeight() - 50;
                }
            }
            else if (Gdx.input.isKeyJustPressed(Keys.UP)) {
                if (selectedOption > 0) {
                    selectedOption --;
                    selectorY += 30;
                }
                else {
                    selectedOption = resources.size - 1;
                    selectorY = Gdx.graphics.getHeight() - 20 - 30 * resources.size;
                }
            }
            if (Gdx.input.isKeyJustPressed(Keys.ENTER) && ship.getHealth() > 0) {
                Resource re = resources.get(selectedOption);
                money += re.getMonetaryValue();
                removeResource(re);
            }
        }
        hudBatch.end();
    }

    private void handleBuyScreen() {
        font.draw(hudBatch, "BUY -- [ARROW KEYS] MOVE [ENTER] BUY [SPACE] CLOSE", 20, Gdx.graphics.getHeight() - 20);
        int y = Gdx.graphics.getHeight() - 50;
        int count = 0;
        for (Component c: ComponentList.getAllSystems()) {
            if (components.size != 0) {
                for (Component co:components)
                {
                    if (co.getName() == c.getName()) {
                        font.setColor(Color.GRAY);
                        continue;
                    }
                    else {
                        font.setColor(Color.WHITE);
                    }
                }
            }
            String s = " [";
            if (c.getPrice().size != 0) {
                for (Resource r:c.getPrice()) {
                    s += r.getName() + "[" + r.getQuantity() + "], ";
                }
            }
            if (c.getMonetaryPrice() != 0) {
                s += c.getMonetaryPrice() + " credits. ";
            }
            s += "]";
            font.draw(hudBatch,c.getName() + s,40,y);
            font.setColor(Color.WHITE);
            y -= 30;
        }
        font.draw(hudBatch,">",20,selectorY);
        String s = ComponentList.getAllSystems().get(selectedOption).getDescription();
        font.draw(hudBatch,s,20,140);
        if (Gdx.input.isKeyJustPressed(Keys.UP)) {
            if (selectedOption > 0) {
                selectedOption --;
                selectorY += 30;
            }
            else {
                selectedOption = ComponentList.getAllSystems().size - 1;
                selectorY = Gdx.graphics.getHeight() - 20 - 30 * ComponentList.getAllSystems().size;
            }
        }
        if (Gdx.input.isKeyJustPressed(Keys.DOWN)) {
            if (selectedOption < ComponentList.getAllSystems().size - 1) {
                selectedOption++;
                selectorY -= 30;
            } else {
                selectedOption = 0;
                selectorY = Gdx.graphics.getHeight() - 47;
            }
        }
        if (Gdx.input.isKeyJustPressed(Keys.ENTER) && ship.getHealth() > 0) {
            Component c = ComponentList.getAllSystems().get(selectedOption);
            if (c.getPrice().size != 0) {
                for (Resource r:c.getPrice())
                {
                    for (Resource re:resources)
                    {
                        if (re.getName() == r.getName() && re.getQuantity() >= r.getQuantity()) {
                            for (int i = 0; i < r.getQuantity(); i++) {
                                removeResource(re);
                            }
                            c.getPrice().removeIndex(c.getPrice().indexOf(r,true));
                        }
                    }
                }
            }
            if (c.getPrice().size <= 0 && money >= c.getMonetaryPrice()) {
                if (c.getName() == "FUEL TANK") {
                    fuelTanks++;
                    c.setName("FUEL TANK");
                }
                else if (c.getName() == "O2 TANK") {
                    oxygenTanks++;
                    c.setName("O2 TANK");
                }
                else {
                    components.add(c);
                    ComponentList.getAllSystems().removeIndex(ComponentList.getAllSystems().indexOf(c,true));
                    System.out.println(components.get(components.size - 1).getName());
                }
                money -= c.getMonetaryPrice();
            }
        }
    }

    private void enemyExtracting(Planet planet, EnemyShip enemy)
    {
        if (planet.getResources().size == 0) {
            planet.setBeingExtracted(false);
            enemy.setExtracting(false);
            enemy.setExtractionTimer(-10);
            enemy.setObjectiveTimer(0);
            enemy.setTarget(null);
            if (enemy.getTarget() == null) {
                enemy.changeTarget();
            }
            return;
        }
        Resource r = planet.getResources().first();
        if (enemy.getExtractionTimer() == -10) {
            enemy.setExtractionTimer(r.getResistance());
        }
        if (enemy.getExtractionTimer() > 0) {
            enemy.tickTimer();
            font.setColor(Color.RED);
            font.draw(batch,"ENEMY EXTRACTING -- \n" + r.getName() + enemy.getExtractionTimer(),planet.getX(),planet.getY() + 180);
            font.setColor(Color.WHITE);
        } else {
            planet.removeResource(r);
            enemy.setExtractionTimer(-10);
        }
    }

    private void extractMineral(Planet planet, Extractor extractor) {

        if (planet.getResources().size == 0) {
            planet.setBeingExtracted(false);
            int index = extractors.indexOf(extractor,true);
            extractors.removeIndex(index);
            return;
        }
        Resource r = planet.getResources().first();
        if (extractor.extractionTimer == -1) {
            if (checkForComponent("NEUROPORT ABSORBER")) {
                extractor.extractionTimer = r.getResistance() / 2;
            }
            else {
                extractor.extractionTimer = r.getResistance();
            }
        }
        if (extractor.extractionTimer > 0)
        {
            extractor.extractionTimer -= Gdx.graphics.getDeltaTime();
            font.draw(batch,"EXTRACTING -- \n" + r.getName() + "[ " + extractor.extractionTimer + " ]",planet.getX(),planet.getY() + 180);
        }
        else {
            addResource(r,planet,extractor);
        }
    }

    public void grabResource(Resource r)
    {
        Boolean b = false;
        for (Resource re:resources) {
            if (re.getName() == r.getName()) {
                re.setQuantity(re.getQuantity() + 1);
                b = true;
            }
        }
        if (!b) {
            resources.add(r);
        }
    }

    public void addResource(Resource r, Planet planet, Extractor extractor)
    {
        Boolean b = false;
        for (Resource re:resources) {
            if (re.getName() == r.getName()) {
                re.setQuantity(re.getQuantity() + 1);
                b = true;
            }
        }
        if (!b) {
            resources.add(r);
        }
        planet.removeResource(r);
        extractor.extractionTimer = -1;
    }

    public void removeResource(Resource r)
    {
        if (r.getQuantity() > 1) {
            r.setQuantity(r.getQuantity() - 1);
        }
        else {
            resources.removeIndex(resources.indexOf(r,true));
        }
    }

    private void handleShipFunctions(Planet p) {
        float distance = Vector2.dst(ship.getX(),ship.getY(), p.getX(),p.getY());
        if (Gdx.input.isKeyPressed(Keys.NUM_1) && distance < 200) {
            String res = "";
            if (p.getResources().size == 0) {
                res = "-NO RESOURCES-";
            }
            else {
                for (int i = 0; i < p.getResources().size; i++) {
                    res += p.getResources().get(i).getName() + " [R " + p.getResources().get(i).getResistance() + "]\n";
                }
            }
            font.setColor(Color.CYAN);
            font.draw(batch,res,p.getX(),p.getY() - 20);
            font.setColor(Color.WHITE);
        }
        if (Gdx.input.isKeyJustPressed(Keys.NUM_2) && distance <= 100 && checkForComponent("NEUTRON ACTIVATOR")) {
            shootExtractor(p);
        }
        if (Gdx.input.isKeyJustPressed(Keys.CONTROL_RIGHT ) || Gdx.input.isKeyJustPressed(Keys.CONTROL_LEFT)) {
            if (ship.getDir() != Direction.STOP && ship.isActive()) {
                lasers.add(new Laser(ship.getX() + 16,ship.getY() + 20,ship.getDir(),game.getAssetManager()));
            }
        }
    }

    private void handleAstronautFunctions(Planet p) {
        float distance = Vector2.dst(player.getX(),player.getY(),p.getX(),p.getY());
        if (Gdx.input.isKeyJustPressed(Keys.NUM_2) && distance <= 100) {
            shootExtractor(p);
        }
    }

    private void shootExtractor(Planet p) {
        if (p.getResources().size != 0 && !p.isBeingExtracted() && extractors.size < maxExtractors) {
            Extractor e = new Extractor(p.getX() + 55,p.getY() + 120,game.getAssetManager());
            extractors.add(e);
            e.extractionTimer = p.getResources().first().getResistance();
        }
    }

    private boolean checkForComponent(String name) {
        if (components.size == 0) {
            return false;
        }
        for (Component c:components) {
            if (c.getName() == name) {
                return true;
            }
        }
        return false;
    }

    public void resize(int width, int height) {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void hide() {
        dispose();
    }
    public void dispose() {
        fade.getTexture().dispose();
        player.dispose();
        ship.dispose();
        shop.dispose();
        gate.dispose();
        for (int i = 0; i < planets.size(); i++) {
            planets.get(i).dispose();
        }
        hudBatch.dispose();
        for (int h = 0; h < extractors.size; h++) {
            extractors.get(h).dispose();
        }
        for (int j = 0; j < meteors.size; j++) {
            meteors.get(j).dispose();
        }
        for (int k = 0; k < lasers.size; k++) {
            lasers.get(k).dispose();
        }
        for (int e = 0; e < enemies.size; e++) {
            enemies.get(e).dispose();
        }
        for (int z = 0; z < resourceContainer.size; z++) {
            resourceContainer.get(z).dispose();
        }
    }
}
